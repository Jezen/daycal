### DayCal

A graphical representation of calendar events for a single day, made with
JavaScript, Browserify, Mocha, Chai, and Flow.

```sh
# Getting started
npm i

# Running the tests
npm test

# Compiling the JS bundle
npm run build

# Static type analysis
npm run typecheck
```
