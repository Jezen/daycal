/* @flow */

module.exports = (function(): Object {
  function hourFromMilitaryToPeriod(time: string): string {
    var segments: Array<string> = time.split(':');
    var hour: number = parseInt(segments[0], 10);
    var minute: string = segments[1];
    var period: string = hour > 11 ? ' PM' : ' AM';

    if (hour === 0) {
      hour = 12;
    }

    if (hour > 12) {
      hour -= 12;
    }

    time = [hour, minute].join(':');

    if (minute === '00') {
      time += period;
    }

    return time;
  }

  function buildTimeRange(start: number, end: number): Array<string> {
    var hours: Array<[string, string]> = [];
    for (var hour: number = start; hour <= end; hour += 1) {
      hours.push([hour + ':00', hour + ':30']);
    }
    return [].concat.apply([], hours).slice(0, -1);
  }

  function listItem(message: string): HTMLElement {
    var li: HTMLElement = document.createElement('li');
    li.innerText = message;
    return li;
  }

  function renderScale(range: Array<HTMLElement>): void {
    var fragment: DocumentFragment = document.createDocumentFragment();

    for (var i: number = 0; i < range.length; i += 1) {
      fragment.appendChild(range[i]);
    }

    document.querySelector('.timeline').appendChild(fragment);
  }

  function isolateCollisionGroups(events) {
    function getEnd(event) {
      return event.end;
    }

    var groups = [];

    events.forEach(function(event): void {
      if (!groups.length) { groups.push([]); }

      var current: number = groups.length - 1;

      if (!groups[current].length) {
        groups[current].push(event);
      } else {
        if (event.start < groups[current].map(getEnd).sort().slice(-1)) {
          groups[current].push(event);
        } else {
          groups.push([event]);
        }
      }
    });

    return groups;
  }

  function flatten(a, b): Array<any> {
    return a.concat(b);
  }

  function applyUniformWidthAndPosition(columns) {
    return columns.map(function(events, index: number) {
      return events.map(function(event) {
        var width: number = 600 / columns.length;
        event.width = width;
        event.left = index * width;
        return event;
      });
    }).reduce(flatten, []);
  }

  function arrangeInColumns(group) {
    var columns = [];

    group.forEach(function(event) {
      if (!columns[0]) { return columns.push([event]); }

      if (columns[0] && !columns[1]) {
        columns.push([]);
        columns[1].push(event);
      } else {
        for (var i: number = 0; i < columns.length; i++) {
          if (columns[i][columns[i].length - 1].end < event.start) {
            columns[i].push(event);
            break;
          } else {
            if (columns.length == i + 1) {
              return columns.push([event]);
            }
          }
        }
      }
    });

    return columns;
  }

  function layOutDay(events) {
    function startTime(a, b): boolean {
      return a.start > b.start;
    }

    function setCoords(event) {
      event.top = event.start;
      event.left += 10;
      return event;
    }

    return isolateCollisionGroups(events.sort(startTime))
      .map(arrangeInColumns)
      .map(applyUniformWidthAndPosition)
      .reduce(flatten, [])
      .map(setCoords);
  }

  function buildEventCardTitle(id): HTMLElement {
    var title: HTMLElement = document.createElement('h2');
    var message = 'Sample Item (#' + id + ')';
    var text: Text = document.createTextNode(message);
    title.appendChild(text);
    return title;
  }

  function buildEventCardSubtitle(): HTMLElement {
    var title: HTMLElement = document.createElement('h3');
    var message: string = 'Sample Location';
    var text: Text = document.createTextNode(message);
    title.appendChild(text);
    return title;
  }

  function buildEventCard(event): HTMLDivElement {
    var card: HTMLDivElement = document.createElement('div');
    var height: number = event.end - event.start;
    card.appendChild(buildEventCardTitle(event.id));
    card.appendChild(buildEventCardSubtitle());
    card.classList.add('card');
    card.style.top = event.top + 'px';
    card.style.left = event.left + 'px';
    card.style.height = height + 'px';
    card.style.width = event.width + 'px';
    return card;
  }

  function renderEvents(events): void {
    var fragment: DocumentFragment = document.createDocumentFragment();
    events.forEach(function(event): void {
      var card: HTMLDivElement = buildEventCard(event);
      fragment.appendChild(card);
    })
    document.querySelector('.calendar').appendChild(fragment);
  }

  function render(events): void {
    renderScale(buildTimeRange(9, 21)
                .map(hourFromMilitaryToPeriod)
                .map(listItem));

    renderEvents(layOutDay(events));
  }

  return {
    hourFromMilitaryToPeriod: hourFromMilitaryToPeriod,
    buildTimeRange: buildTimeRange,
    listItem: listItem,
    render: render,
  };
});
