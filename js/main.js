/* @flow */

var Calendar = require('./calendar.js');

module.exports = (function() {
  type CalendarEvent = { id: number, start: number, end: number };

  var calendar = Calendar();
  var events: Array<CalendarEvent> = [
    { id: 1, start: 30, end: 150 },
    { id: 2, start: 540, end: 600 },
    { id: 3, start: 560, end: 620 },
    { id: 4, start: 610, end: 670 }
  ];

  calendar.render(events);
})();
