var Calendar = require('../js/calendar.js');
var expect = require('chai').expect;

describe('Calendar', function() {
  var calendar = Calendar();

  describe('#hourFromMilitaryToPeriod', function() {
    it('converts hours less than 12 to ante meridiem', function() {
      expect(calendar.hourFromMilitaryToPeriod('09:00')).to.equal('9:00 AM');
    });

    it('converts hours more than 12 to post meridiem', function() {
      expect(calendar.hourFromMilitaryToPeriod('23:00')).to.equal('11:00 PM');
    });

    it('converts midnight to ante meridiem', function() {
      expect(calendar.hourFromMilitaryToPeriod('00:00')).to.equal('12:00 AM');
    });

    it('converts noon to post meridiem', function() {
      expect(calendar.hourFromMilitaryToPeriod('12:00')).to.equal('12:00 PM');
    });

    it('ignores the period if the time is not on the hour', function() {
      expect(calendar.hourFromMilitaryToPeriod('12:30')).to.equal('12:30');
    });
  });

  describe('#buildTimeRange', function() {
    // A nice feature of reasonable languages is first-class support for
    // ranges, e.g., [1..10]. ECMAScript 5 is not one of those languages.
    it('builds an inclusive time range with 30 minutes intervals', function() {
      var range = ['9:00', '9:30', '10:00', '10:30', '11:00'];
      expect(calendar.buildTimeRange(9, 11)).to.deep.equal(range);
    });
  });

  describe('#listItem', function() {
    it('creates a list item', function() {
      var listItem = calendar.listItem();
      expect(listItem.tagName).to.equal('LI');
    });

    it('injects a custom message into the list item', function() {
      var listItem = calendar.listItem('foo');
      expect(listItem.innerText).to.equal('foo');
    });
  });
});
